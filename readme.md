# Universal Document Converter
##### A simple Java wrapper for Pandoc

## What is Universal Document Converter?
Universal Document Converter (UDC) is an utility that converts documents from one
format into another.

UDC is a Java wrapper for Pandoc, a great tool that performs lots of different
conversions.

### What is Pandoc?
[Pandoc](http://pandoc.org) is a standalone command-line program that can convert documents in markdown,
HTML, LaTeX, MediaWiki markup, TWiki markup, Microsoft Word docx, LibreOffice ODT, EPUB and many more file formats.

Pandoc is free software, released under the GPL, and available for Windows, Linux and Mac OS X.

## Install
You need Java 8 and [Pandoc installed](http://pandoc.org/installing.html) in your system.

### Maven installation

```
#!xml
<dependency>
    <groupId>org.bitbucket.leito</groupId>
    <artifactId>universal-document-converter</artifactId>
    <version>1.1.0</version>
</dependency>
```

### Pandoc installation
Read about [installing Pandoc](http://pandoc.org/installing.html) (it's just executing an installer for Windows, Linux or Mac).

## Usage

```
#!java
 new DocumentConverter()
     .fromFile(new File("sample.odt"), InputFormat.ODT)
     .toFile(new File("sample.html"), OutputFormat.HTML5)
     .addOption("-s")                     //optional
     .workingDirectory(new File("/tmp"))  //optional
     .convert();
```

Also, the class DocumentConverterTest.java has several usage examples.

## License
Universal Document Converter is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at [Mozilla Public License FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/).